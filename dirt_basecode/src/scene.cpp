/*
	This file is part of Dirt, the Dartmouth introductory ray tracer, used in
	Dartmouth's COSC 77/177 Computer Graphics course.

	Copyright (c) 2018 by Wojciech Jarosz

	Dirt is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License Version 3
	as published by the Free Software Foundation.

	Dirt is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "scene.h"
#include "progress.h"

uint64_t intersection_tests = 0;
uint64_t rays_traced = 0;

Scene::~Scene()
{
	delete m_surfaces;
	delete m_camera;
	for (auto m : m_materials)
		delete m.second;
	m_materials.clear();
}

// compute the color corresponding to a ray by raytracing
Color3f Scene::recursiveColor(const Ray3f &ray, int depth) const
{
	const int maxDepth = 64;

	HitInfo hit;
	if (intersect(ray, hit)) {
		Ray3f scattered;
		Color3f attenuation;
		Color3f emitted = hit.mat->emitted(ray, hit);
		if (depth < maxDepth && hit.mat->scatter(ray, hit, attenuation, scattered))
			return emitted + attenuation * recursiveColor(scattered, depth + 1);
		else
			return emitted;
	}
	else {
		return m_background;
	}

}

// raytrace an image
Image3f Scene::raytrace() const
{
	// allocate an image of the proper size
	auto image = Image3f(m_camera->resolution().x(), m_camera->resolution().y());

	Progress progress("Rendering", m_camera->resolution().x() * m_camera->resolution().y());
	// foreach pixel
#pragma omp parallel for schedule(dynamic, 8)
	for (int j = 0; j < m_camera->resolution().y(); j++)
	{
		for (auto i : range(m_camera->resolution().x()))
		{
			// init accumulated color
			image(i, j) = Color3f::Zero();

			// foreach sample
			for (int s = 0; s < m_imageSamples; ++s)
			{
				// set pixel to the color raytraced with the ray
				INCREMENT_TRACED_RAYS;
				image(i, j) += recursiveColor(m_camera->generateRay(i + randf(), j + randf()), 0);
			}
			// scale by the number of samples
			image(i, j) /= float(m_imageSamples);

			++progress;
		}
	}

	// return the ray-traced image
	return image;
}
