/*
	This file is part of Dirt, the Dartmouth introductory ray tracer, used in
	Dartmouth's COSC 77/177 Computer Graphics course.

	Copyright (c) 2018 by Wojciech Jarosz

	Dirt is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License Version 3
	as published by the Free Software Foundation.

	Dirt is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "bbh.h"

bool aabbIntersect(const AABB3f &bounds, const Ray3f &ray)
{
	float minT = ray.mint;
	float maxT = ray.maxt;

	for (int i = 0; i < 3; ++i) {
		float invD = 1.0f / ray.d[i];
		float t0 = (bounds[0][i] - ray.o[i]) * invD;
		float t1 = (bounds[1][i] - ray.o[i]) * invD;
		if (invD < 0.0f)
			std::swap(t0, t1);

		minT = t0 > minT ? t0 : minT;
		maxT = t1 < maxT ? t1 : maxT;
		if (maxT < minT)
			return false;
	}
	return true;

}

BBHNode::BBHNode(std::vector<Surface *> primitives)
	: m_left(nullptr),
	m_right(nullptr)
{
	if (primitives.size() == 1) {
		m_left = m_right = primitives[0];
	}
	else if (primitives.size() == 2) {
		m_left = primitives[0];
		m_right = primitives[1];
	}
	else {
		int axis = int(randf() * 3.0f);
		std::sort(primitives.begin(), primitives.end(), [&](const Surface * a, const Surface * b) {
			return a->worldBBox().center()[axis] < b->worldBBox().center()[axis];
		});

		std::vector<Surface*> leftList, rightList;
		leftList.insert(leftList.begin(), primitives.begin(), primitives.begin() + primitives.size() / 2);
		rightList.insert(rightList.begin(), primitives.begin() + primitives.size() / 2, primitives.end());

		m_left = new BBHNode(leftList);
		m_right = new BBHNode(rightList);
	}

	m_bounds = m_left->worldBBox().merged(m_right->worldBBox());
}

BBHNode::~BBHNode()
{
	if (m_left)
		delete m_left;
	if (m_right && m_right != m_left)
		delete m_right;
}

bool BBHNode::intersect(const Ray3f &ray, HitInfo &hit) const
{
	if (aabbIntersect(m_bounds, ray)) {
		HitInfo leftHit, rightHit;
		bool hitLeft = m_left->intersect(ray, leftHit);
		bool hitRight = m_right->intersect(ray, rightHit);

		if (hitLeft && hitRight) {
			if (leftHit.t < rightHit.t)
				hit = leftHit;
			else
				hit = rightHit;
			return true;
		}
		else if (hitLeft) {
			hit = leftHit;
			return true;
		}
		else if (hitRight) {
			hit = rightHit;
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

// BBH Method Definitions
BBH::BBH(const Scene & scene, const json & j) : SurfaceGroup(scene, j)
{
	// These values aren't used in the base code right now - but you can use these for when you want to extend
	// the basic BVH functionality

	int maxSurfsInNode = 10;
	maxSurfsInNode = j.value("maxPrimsInNode", maxSurfsInNode);

	string sm("sah");
	sm = j.value("splitMethod", sm);
	if (sm == "sah") {
		// Surface-area heuristic
	}
	else if (sm == "middle") {
		// Split at the center of the bounding box
	}
	else if (sm == "equal") {
		// Split so that an equal number of objects are on either side
	}
}

BBH::~BBH()
{
	if (m_root)
		delete m_root;
}

void BBH::build()
{
	if (!m_surfaces.empty())
		m_root = new BBHNode(m_surfaces);
	else
		m_root = nullptr;
}

bool BBH::intersect(const Ray3f &ray, HitInfo &hit) const
{
	if (m_root)
		return m_root->intersect(ray, hit);
	else
		return false;
}
