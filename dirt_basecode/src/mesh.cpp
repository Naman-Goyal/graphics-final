/*
	This file is part of Dirt, the Dartmouth introductory ray tracer, used in
	Dartmouth's COSC 77/177 Computer Graphics course.

	Copyright (c) 2018 by Wojciech Jarosz

	Dirt is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License Version 3
	as published by the Free Software Foundation.

	Dirt is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "mesh.h"
#include "image.h"

//From class, matrix method
bool namanTriangleIntersect(const Ray3f& ray,
	const Vector3f& p0, const Vector3f& p1, const Vector3f& p2,
	const Normal3f* n0, const Normal3f* n1, const Normal3f* n2,
	const Point2f* uv0, const Point2f* uv1, const Point2f* uv2,
	HitInfo& its,
	const Material * material,
	const Surface * surface) {
	Eigen::Matrix3f dab;
	dab.col(0) = -1 * ray.d;
	dab.col(1) = p0 - p2;
	dab.col(2) = p1 - p2;
	Vector3f tab = dab.inverse() * (ray.o - p2);
	float t = tab.x();
	float a = tab.y();
	float b = tab.z();
	float g = 1 - a - b;
	//See if outside triangle
	if (a > 1 || a < 0 || b > 1 || b < 0 || g > 1 || g < 0) {
		return false;
	}
	if (t < ray.mint || t > ray.maxt) {
		return false;
	}

	Vector3f gn = (p1 - p0).cross(p2 - p0).normalized();

	Vector3f sn;
	if (n0 != nullptr && n1 != nullptr && n2 != nullptr)
		sn = (a * (*n0) + b * (*n1) + g * (*n2)).normalized();
	else
		sn = gn;

	//Stuff for UVs

	Point2f uv(0,0);
	if (uv0 != nullptr && uv1 != nullptr && uv2 != nullptr)
		uv = a * (*uv0) + b * (*uv1) + g * (*uv2);
	else {
		Vector3f edge1 = p1 - p0, edge2 = p2 - p0;
		Vector3f pvec = ray.d.cross(edge2);
		float inv_det = 1.0f / edge1.dot(pvec);
		Vector3f tvec = ray.o - p0;
		float u = tvec.dot(pvec) * inv_det;
		Vector3f qvec = tvec.cross(edge1);
		float v = ray.d.dot(qvec) * inv_det;
		uv = Point2f(u, v);
	}

	its = HitInfo(t, ray(t), gn, sn, uv, material, surface);
	return true;
}

// Ray-Triangle intersection
// p0, p1, p2 - Triangle vertices
// n0, n1, n2 - optional per vertex normal data
bool singleTriangleIntersect(const Ray3f& ray,
	const Vector3f& p0, const Vector3f& p1, const Vector3f& p2,
	const Normal3f* n0, const Normal3f* n1, const Normal3f* n2,
	const Point2f* uv0, const Point2f* uv1, const Point2f* uv2,
	HitInfo& its,
	const Material * material,
	const Surface * surface)
{
	// Find vectors for two edges sharing v[0]
	Vector3f edge1 = p1 - p0,
		edge2 = p2 - p0;

	// Begin calculating determinant - also used to calculate U parameter
	Vector3f pvec = ray.d.cross(edge2);

	// If determinant is near zero, ray lies in plane of triangle
	float det = edge1.dot(pvec);

	if (det > -1e-8f && det < 1e-8f)
		return false;
	float inv_det = 1.0f / det;

	// Calculate distance from v[0] to ray origin
	Vector3f tvec = ray.o - p0;

	// Calculate U parameter and test bounds
	float u = tvec.dot(pvec) * inv_det;
	if (u < 0.0 || u > 1.0)
		return false;

	// Prepare to test V parameter
	Vector3f qvec = tvec.cross(edge1);

	// Calculate V parameter and test bounds
	float v = ray.d.dot(qvec) * inv_det;
	if (v < 0.0 || u + v > 1.0)
		return false;

	// Ray intersects triangle -> compute t
	float t = edge2.dot(qvec) * inv_det;

	if (!(t >= ray.mint && t <= ray.maxt))
		return false;

	Vector3f gn = (p1 - p0).cross(p2 - p0).normalized();

	Vector3f bary(1 - (u + v), u, v);

	Vector3f sn;
	if (n0 != nullptr && n1 != nullptr && n2 != nullptr)
		sn = (bary.x() * (*n0) +
			bary.y() * (*n1) +
			bary.z() * (*n2)).normalized();
	else
		sn = gn;

	Point2f uv(u, v);
	if (uv0 != nullptr && uv1 != nullptr && uv2 != nullptr)
		uv = (bary.x() * (*uv0) + bary.y() * (*uv1) + bary.z() * (*uv2));

	// if hit, set intersection record values
	its = HitInfo(t, ray(t), gn, sn, uv, material, surface);
	return true;
}

Triangle::Triangle(const Scene & scene, const json & j, Mesh * mesh, int triNumber)
	: Surface(scene, j), m_mesh(mesh), m_face(&mesh->F[triNumber])
{
}

bool Triangle::intersect(const Ray3f &ray, HitInfo &hit) const
{
	INCREMENT_INTERSECTION_TESTS;

	auto i0 = m_face->x(), i1 = m_face->y(), i2 = m_face->z();
	const Point3f p0 = m_mesh->V[i0], p1 = m_mesh->V[i1], p2 = m_mesh->V[i2];

	const Normal3f * n0 = nullptr, *n1 = nullptr, *n2 = nullptr;
	if (!m_mesh->N.empty())
	{
		n0 = &m_mesh->N[i0];
		n1 = &m_mesh->N[i1];
		n2 = &m_mesh->N[i2];
	}

	const Point2f* uv0 = nullptr, *uv1 = nullptr, *uv2 = nullptr;
	if (!m_mesh->UV.empty())
	{
		uv0 = &m_mesh->UV[i0];
		uv1 = &m_mesh->UV[i1];
		uv2 = &m_mesh->UV[i2];
	}

	return singleTriangleIntersect(
		ray,
		p0, p1, p2,
		n0, n1, n2,
		uv0, uv1, uv2,
		hit, m_material, this);
}

AABB3f Triangle::localBBox() const
{
	// all mesh vertices have already been transformed to world space,
	// so we need to transform back to get the local space bounds
	AABB3f result;
	result.extend(m_xform.inverse() * m_mesh->V[m_face->x()]);
	result.extend(m_xform.inverse() * m_mesh->V[m_face->y()]);
	result.extend(m_xform.inverse() * m_mesh->V[m_face->z()]);
	for (int i = 0; i < 3; ++i) {
		if (result[1][i] - result[0][i] < 1e-4f) {
			result[0][i] -= 5e-5f;
			result[1][i] += 5e-5f;
		}
	}
	return result;
}

AABB3f Triangle::worldBBox() const
{
	// all mesh vertices have already been transformed to world space,
	// so just bound the triangle vertices
	AABB3f result;
	result.extend(m_mesh->V[m_face->x()]);
	result.extend(m_mesh->V[m_face->y()]);
	result.extend(m_mesh->V[m_face->z()]);
	for (int i = 0; i < 3; ++i) {
		if (result[1][i] - result[0][i] < 1e-4f) {
			result[0][i] -= 5e-5f;
			result[1][i] += 5e-5f;
		}
	}
	return result;
}

void Mesh::apply_bump_map(std::string path, int size)
{
	Image3f map_img(size, size);
	map_img.load(path);

	for (int i = 0; i < V.size(); i++) {
		int px = static_cast<int>(UV[i](0) * (size-1));
		int py = static_cast<int>(UV[i](1) * (size - 1));
		float value = map_img(px, py).r();
		V[i] = V[i] + (N[i] * bump_height * value);
	}
}
