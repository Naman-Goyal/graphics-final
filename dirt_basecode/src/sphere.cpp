/*
	This file is part of Dirt, the Dartmouth introductory ray tracer, used in
	Dartmouth's COSC 77/177 Computer Graphics course.

	Copyright (c) 2018 by Wojciech Jarosz

	Dirt is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License Version 3
	as published by the Free Software Foundation.

	Dirt is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "sphere.h"



Sphere::Sphere(float radius,
	const Material * material,
	const Transform & xform)
	: Surface(material, xform), m_radius(radius)
{

}

Sphere::Sphere(const Scene & scene, const json & j)
	: Surface(scene, j)
{
	m_radius = j.value("radius", m_radius);
}

AABB3f Sphere::localBBox() const
{
	return AABB3f(-m_radius * Vector3f::Ones(), m_radius*Vector3f::Ones());
}


bool Sphere::intersect(const Ray3f &ray, HitInfo &hit) const
{
	INCREMENT_INTERSECTION_TESTS;

	// compute ray intersection (and ray parameter), continue if not hit
	// just grab only the first hit
	auto tray = m_xform.inverse() * ray;
	auto a = tray.d.squaredNorm();
	auto b = 2 * tray.d.dot(tray.o);
	auto c = tray.o.squaredNorm() - m_radius * m_radius;

	// solve the quadratic equation using double precision
	double discrim = (double)b * (double)b - 4 * (double)a * (double)c;
	if (discrim < 0)
		return false;

	double rootDiscrim = std::sqrt(discrim);

	double q = (b < 0) ? -.5 * (b - rootDiscrim) : -.5 * (b + rootDiscrim);

	float t1 = float(q / a);
	float t2 = float(c / q);
	if (t1 > t2)
		std::swap(t1, t2);

	// compute t
	float t = (t1 < tray.mint) ? t2 : t1;

	// check if computed param is within ray.mint and ray.maxt
	if (t < tray.mint || t > tray.maxt)
		return false;

	auto p = tray(t);
	p *= m_radius / p.norm();

	Normal3f gn = (m_xform * Normal3f(p)).normalized();
	// if hit, set intersection record values
	hit = HitInfo(t, m_xform * p, gn, gn, Point2f(0, 0), m_material, this);
	return true;
}
