/*
	Naman Goyal
	26 May 2019
	Main scene maker / file that brings everything else together

	Final Project for Graphics!!
*/
#include "Worley.h"
#include "scene.h"

void smoothstep(Color3f &x) {
	float oldval = x.r();
	float newval = 3 * pow(oldval, 2) - 2 * pow(oldval, 3);
	x = Color3f(newval);
}

void sq(Color3f &x) {
	x = Color3f(x.r() * x.r());
}

void invert(Color3f &x) {
	x = Color3f(1. - x.r());
}

void add(Color3f &x) {
	x *= 0.8;
	x += 0.2;
}

int main(int argc, char **argv) {

	//Noise Generate
	//Worley noise(1024, 1024, 16, 8, { 1 });
	//noise.save_with_mods("worley_disp.png", { invert, smoothstep, sq });
	//noise.save_with_mods("worley_width.png", { invert, smoothstep, sq, add });

	//Scene Generate
	std::string scene_file = "newscene.json";
	std::string output_file = "output.png";

	Scene scene(scene_file);
	Image3f img = scene.raytrace();
	img.save(output_file);

	return 0;
}