#ifndef __LoopSubdivision_h__
#define __LoopSubdivision_h__
#include <unordered_map>
#include <unordered_set>
#include "Mesh.h"

/* vtx_vtx_map_make
 * Inputs:
 * 	int vtx_n                      - number of vertices
 * 	Array<Vector3i>& tris          - all the triangles
 * Returns:
 * 	Hashtable<int, Hashset<int>>   - a map from each vertex to its neighbors
 */
inline Hashtable<int, Hashset<int>> vtx_vtx_map_make(size_t vtx_n, Array<Vector3i>& tris) {
	Hashtable<int, Hashset<int>> map;

	// Init Hashsets for each vertex
	for (int i = 0; i < vtx_n; i++) {
		map[i] = Hashset<int>();
	}

	// Give all connection into the hashset, which should take care of duplicates
	for (int i = 0; i < tris.size(); i++) {
		int v0 = tris[i](0);
		int v1 = tris[i](1);
		int v2 = tris[i](2);

		map[v0].insert(v1);
		map[v0].insert(v2);
		map[v1].insert(v0);
		map[v1].insert(v2);
		map[v2].insert(v0);
		map[v2].insert(v1);
	}

	return map;
}

/* edge_tri_map_make
 * Inputs:
 * 	Array<Vector3i>& tris            - all the triangles
 * Returns:
 * 	Hashtable<Vector2i, Array<int>>  - a map from each edge to the triangles it's in
 */
inline Hashtable<Vector2i, Array<int>> edge_tri_map_make(Array<Vector3i>& tris) {
	Hashtable<Vector2i, Array<int>> map;
	for (int i = 0; i < tris.size(); i++) {
		for (int j = 0; j < 3; j++) {
			Vector2i es = Sorted(Vector2i(tris[i](j), tris[i]((j+1) % 3)));

			// If map doesn't have this edge, init an array
			if (!map.count(es)) {
				map.insert({es, Array<int>()});
			}
			map[es].push_back(i);
		}
	}

	return map;
}

/* opposite_vtx
 * Inputs:
 * 	Vector2i edge             - two indices for vertices of the edge
 * 	Vector3i tri              - three indices for vertices of the face
 * Returns:
 * 	int						  - index of the opposing vertex
 */
inline int opposite_vtx(Vector2i edge, Vector3i tri) {
	for (int i = 0; i < 3; i++) {
		if (tri(i) != edge(0) && tri(i) != edge(1)) {
			return tri(i);
		}
	}
	// Should never happen
	return -1;
}

/* even_vtx_weighted_average
 * Inputs:
 * 	int vtx								- current vertex index
 * 	Hashset<int> neighbors				- indices of all neighbor vertices
 * 	Array<Vector3>& old_vtx				- all the verts
 */
inline Vector3 even_vtx_weighted_average(int vtx, Hashset<int> neighbors, Array<Vector3>& old_vtx) {
	unsigned int n = neighbors.size();
	double b = (n > 3) ? 3/(8.0*n) : 3/16.0;

	Vector3 new_loc = (1-n*b) * old_vtx[vtx];
	for (auto it : neighbors) {
		new_loc += b * old_vtx[it];
	}

	return new_loc;
}

inline void LoopSubdivision(TriangleMesh<3>& mesh)
{
	Array<Vector3>& old_vtx = mesh.Vertices();
	Array<Vector3i>& old_tri = mesh.Elements();
	Array<Vector3> new_vtx;		////vertex array for the new mesh
	Array<Vector3i> new_tri;	////element array for the new mesh
	
	new_vtx=old_vtx;	////copy all the old vertices to the new_vtx array

	// Neighbour / relationship maps
	auto edge_tri_map     = edge_tri_map_make(old_tri);
	auto vtx_vtx_map      = vtx_vtx_map_make(old_vtx.size(), old_tri);
	auto edge_odd_vtx_map = Hashtable<Vector2i, int>();

	////step 1: add mid-point vertices and triangles
	////for each old triangle, 
	////add three new vertices (in the middle of each edge) to new_vtx 
	////add four new triangles to new_tri

	for (int i = 0; i < old_tri.size(); i++) {
		Vector3i midpts;
		// Add in and retrieve all midpoints
		for (int j = 0; j < 3; j++) {
			Vector2i es = Sorted(Vector2i(old_tri[i][j], old_tri[i][(j+1) % 3]));

			//If we haven't already added in midpoint, do so and store it as a mid point
			auto midpt_iterator = edge_odd_vtx_map.find(es);
			if (midpt_iterator == edge_odd_vtx_map.end()) {
				new_vtx.push_back((old_vtx[es(0)] + old_vtx[es(1)]) / 2);
				edge_odd_vtx_map[es] = midpts(j) = new_vtx.size() - 1;
			}
			// If midpt was already made, just get it
			else {
				midpts(j) = midpt_iterator->second;
			}

			//Add in the triangles

		}

		// Make new triangles
		new_tri.push_back(midpts);
		for (int j = 0; j < 3; j++) {
			new_tri.push_back({old_tri[i](j), midpts(j), midpts((j+2) % 3)});
		}
	}

	////step 2: update the position for each new mid-point vertex: 
	////for each mid-point vertex, find its two end-point vertices A and B, 
	////and find the two opposite-side vertices on the two incident triangles C and D,
	////then update the new position as .375*(A+B)+.125*(C+D)

	for (auto it : edge_odd_vtx_map) {
		Vector3& odd_vert = new_vtx[it.second];
		Vector3i t0 = old_tri[edge_tri_map[it.first][0]];
		Vector3i t1 = old_tri[edge_tri_map[it.first][1]];

		Vector3 opp_v0  = old_vtx[opposite_vtx(it.first, t0)];
		Vector3 opp_v1  = old_vtx[opposite_vtx(it.first, t1)];

		odd_vert = odd_vert * .75 + (opp_v0 + opp_v1) * .125;
	}

	////step 3: update vertices of each old vertex
	////for each old vertex, find its incident old vertices, and update its position according its incident vertices

	Array<Vector3> temp_vtx(old_vtx.size());
	for (auto it : vtx_vtx_map) {
		temp_vtx[it.first] = even_vtx_weighted_average(it.first, it.second, old_vtx);
	}
	// Copy over the temp verts
	std::copy(temp_vtx.begin(), temp_vtx.end(), new_vtx.begin());

	////update subdivided vertices and triangles onto the input mesh
	old_vtx=new_vtx;
	old_tri=new_tri;
}

#endif