#include <map>
#include "common.h"
#include "image.h"
#include <limits>

class Worley {
private:
	int _imw, _imh;						//Width and height of image
	int _nx, _ny;						//Number of cells in x and y directions
	float _cw, _ch;						//width and height of cell
	std::map<pair<int, int>, pair<float, float>> _feat_pts;// Positions of points, sorted by cell index
	std::vector<int> _fs;				//representation of what fractals we want.
										//Eg {1} would be f1, {-1,1} would be -f1+f2
	Image3f _img;						//Stored image of the noise


	//Generates Feature Points according to the given parameters
	void _generateFeaturePoints(void);
	//Generates one pixel
	float _genOnePix(float &min, float &max, int x, int y);
	//Generates the Image3f for the worley noise
	void _generateImage(void);
	//Distance between two pts
	float _dist(float x1, float y1, float x2, float y2);
	//Modulo, including for negative numbers. n|m
	int _mod(int n, int m);
public:
	//Constructor
	//Imw and imh are image width and height
	//nx and ny are the number of cells in the x and y directions
	//fs is a representation of what fractals we want. {1} would be f1, {-1,1} would be -f1+f2
	Worley::Worley(int imw, int imh, int nx = 10, int ny = 10, std::vector<int> fs = { 1 });

	void save_with_mods(std::string filePath, std::vector<std::function<void(Color3f&)>> funcs);

	//Saves the image at filePath
	void save_image(std::string filePath);

	//returns the float value at the pixel (x,y)
	Color3f get_val_at_pt(int x, int y);
};