#pragma once
#include "Material.h"
#include <memory>

//  Has fresnel reflections / refractions, and attenuates with Beer's Law
class Fresnel : public Material
{
private:
	float _ior;
	Texture *_attenuation;
	Texture *_width;
	Texture *_roughness;
public:
	Fresnel(const json & j = json::object(), const Scene* scene = nullptr);

	bool scatter(const Ray3f &ray, const HitInfo &hit, Color3f &attenuation, Ray3f &scattered) const override;
};