#include "Fresnel.h"
#include "parser.h"
#include "scene.h"
#include "texture.h"
#include <cmath>
#include "rand.h"

inline float schlickFresnel(float cosine, float ior)
{
	float r0 = (1 - ior) / (1 + ior);
	r0 *= r0;
	float x = 1 - cosine;
	return r0 + (1 - r0) * x * x * x * x * x;
}

inline Vector3f refract(const Vector3f &v, const Vector3f &n, float ior)
{
	Vector3f uv = v.normalized();
	float cosV = -1 * n.dot(uv);
	float invIor = 1.f / ior;
	return invIor * uv + (invIor*cosV - sqrt(1 + invIor * invIor * (1 - cosV * cosV))) * n;
}

inline Vector3f reflect(const Vector3f &v, const Vector3f &n)
{
	return v - 2 * v.dot(n) * n;
}

inline Color3f attenuate(Color3f &attenuation, float width) {
	float attnR = exp(-1 * width * attenuation.r());
	float attnG = exp(-1 * width * attenuation.g());
	float attnB = exp(-1 * width * attenuation.b());
	return Color3f(attnR, attnG, attnB);
}

Fresnel::Fresnel(const json & j, const Scene * scene)
{
	_ior = j.value("ior", 1.0);
	_attenuation = Texture::parse("attenuation", j, Color3f(0.5f), scene);
	_width = Texture::parse("width", j, Color3f(0.5f), scene);
	_roughness = Texture::parse("roughness", j, Color3f(0.1f), scene);
}

bool Fresnel::scatter(const Ray3f & ray, const HitInfo & hit, Color3f & attenuation, Ray3f & scattered) const
{
	//Attenuate w/ Beer's Law
	attenuation = attenuate(_attenuation->sample(hit), _width->sample(hit).r());

	float ior = _ior;
	Vector3f normal = hit.sn;
	float schlickCos = -normal.dot(ray.d);	//Incident Angle

	float reflectance;
	//See if the ray is from the inside of the object to air, and get reflectance
	if (schlickCos < 0) {	
		float sinT_sq = ior * ior * (1 - schlickCos * schlickCos);
		ior = 1 / ior;			//IOR is inverted
		if (sinT_sq > 1) {		//Total Internal Reflectance
			reflectance = 1;
		}
		else {
			schlickCos = sqrt(1 + sinT_sq);					//Use Refracted Angle instead
			reflectance = schlickFresnel(schlickCos, ior);	//Get reflectance
		}
		normal *= -1;	//Normal is inverted
	}
	else {
		reflectance = schlickFresnel(schlickCos, ior);
	}

	//Reflect or refract w/ some randomness
	Vector3f d = _roughness->sample(hit).r() * randomInUnitSphere().normalized();
	if (randf() < reflectance) {
		d += reflect(ray.d, normal);
	}
	else {
		d += refract(ray.d, normal, ior);
	}
	scattered = Ray3f(hit.p, d);
	
	return true;
}
