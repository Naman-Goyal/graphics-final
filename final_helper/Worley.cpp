#include "Worley.h"
#include <set>
#include <algorithm>
#include "rand.h"

Worley::Worley(int imw, int imh, int nx, int ny, std::vector<int> fs)
{
	_imw = imw;
	_imh = imh;
	_nx = nx;
	_ny = ny;
	_fs = fs;
	_cw = (float) imw / nx;
	_ch = (float) imh / ny;

	_img = Image3f(imw, imh);

	_generateFeaturePoints();
	_generateImage();
}

void Worley::_generateFeaturePoints(void)
{
	for (int x = 0; x < _nx; x++) {
		for (int y = 0; y < _ny; y++) {
			//Key is the cell index
			std::pair<int, int> key = { x,y };
			//Value is random location somewhere in it
			std::pair<float, float> val = { (randf() + x) * _cw, (randf() + y) * _ch };

			_feat_pts[key] = val;
		}
	}
}

float Worley::_genOnePix(float &min, float &max, int x, int y)
{
	//Find what cell we're in
	std::pair<int, int> cell = { x / _cw, y / _ch };
	std::set<float> mins;

	int n = _fs.size();	//Max feature fractal

	//For every cell in a certain area
	for (int ix = cell.first - n; ix <= cell.first + n; ix++) {
		for (int iy = cell.second - n; iy <= cell.second + n; iy++) {
			//Wrap around the edges
			int wx = _mod(ix, _nx);
			int wy = _mod(iy, _ny);
			//Get the distance for the point in this cell, insert it in the set
			auto found = _feat_pts.find({ wx, wy });
			if (found != _feat_pts.end()) {
				auto val = found->second;
				mins.insert(_dist(val.first, val.second, (float)x, (float)y));
			}
		}
	}

	float sum = 0;

	//For every value in the fractal, add its contribution
	auto iterator = mins.begin();
	for (int i = 0; i < n; i++) {
		float feat = _fs[i];
		float dist = *iterator;
		sum += dist * feat;

		iterator++;
	}

	if (sum < min) min = sum;
	if (sum > max) max = sum;

	return sum;
}

void Worley::_generateImage()
{
	float min = std::numeric_limits<float>::max();
	float max = std::numeric_limits<float>::min();

	for (auto y : range(_imh)) {
		for (auto x : range(_imw)) {
			float res = _genOnePix(min, max, x, y);
			_img(x, y) = Color3f(res);
		}
	}

	//Rescale values by min and max values and clamp
	float scale = std::min(_cw,max - min);
	for (int i = 0; i < _img.size(); i++) {
		float newval = (_img(i).r() - min) / scale;
		newval = std::max(std::min(newval, 1.f), 0.f);
		_img(i) = Color3f(newval);
	}
}

float Worley::_dist(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

int Worley::_mod(int n, int m)
{
	return (n%m + m) % m;
}

void Worley::save_image(std::string filepath) {
	_img.save(filepath);
}

void Worley::save_with_mods(std::string filePath, std::vector<std::function<void(Color3f&)>> funcs)
{
	Image3f temp_img = _img;
	for (int i = 0; i < _img.size(); i++) {
		for (int j = 0; j < funcs.size(); j++) {
			funcs[j](temp_img(i));
		}
	}
	temp_img.save(filePath);
}

Color3f Worley::get_val_at_pt(int x, int y) {
	return _img(x, y);
}