cmake_minimum_required(VERSION 3.1)
project(helper)

set(CompilerFlags
	CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
	CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO
	CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
	CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO)

foreach(CompilerFlag ${CompilerFlags})
	string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
endforeach()

set(dirt_dir ${CMAKE_SOURCE_DIR}/dirt_basecode/src)

set(h_headers
	Worley.h
	Fresnel.h)

set (dirt_headers
	${dirt_dir}/common.h
	${dirt_dir}/rand.h
	${dirt_dir}/image.h)

set(h_srcs
	Worley.cpp
	Fresnel.cpp)

add_library(help_lib STATIC
	${h_headers}
	${h_srcs}
	${dirt_headers})

target_link_libraries(help_lib dirt_lib)

target_include_directories(help_lib PUBLIC
	${PROJECT_SOURCE_DIR}
	${dirt_dir})

SOURCE_GROUP("help\\header files" FILES ${h_headers})
SOURCE_GROUP("help\\source files" FILES ${h_srcs})